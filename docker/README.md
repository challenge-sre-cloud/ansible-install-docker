Docker
=========

An Ansible Role that installs Docker on Linux.

Requirements
------------

None.

Role Variables
--------------

Available variables are listed below, along with default values (see defaults/main.yml):

You can control the state of the docker service, and whether it should start on boot. If you're installing Docker inside a Docker container without systemd or sysvinit, you should set docker_service_manage to false.

```
docker_install_compose_plugin: false
docker_compose_package: docker-compose-plugin
docker_compose_package_state: present
```

You can control whether the package is installed, uninstalled, or at the latest version by setting docker_package_state to present, absent, or latest, respectively. Note that the Docker daemon will be automatically restarted if the Docker package is updated. This is a side effect of flushing all handlers (running any of the handlers that have been notified by this and any other role up to this point in the play).

```
docker_package_state: latest
docker_update_cache: true
```

The main Docker repo URL, common between Debian systems.

```
docker_apt_release_channel: stable
docker_apt_arch: "{{ 'arm64' if ansible_architecture == 'aarch64' else 'amd64' }}"
docker_apt_repository: "deb [arch={{ docker_apt_arch }}] {{ docker_repo_url }}/{{ ansible_distribution | lower }} {{ ansible_distribution_release }} {{ docker_apt_release_channel }}"
docker_apt_ignore_key_error: true
docker_apt_gpg_key: "{{ docker_repo_url }}/{{ ansible_distribution | lower }}/gpg"
```

A list of users who will be added to the docker group.

```
docker_users:
  - <user>
  - <user>
```

Dependencies
------------

None.

Example Playbook
----------------

```
---
- name: Starting docker installation
  hosts: all
  become: true
  roles:
  - { role: docker }

```

License
-------

MIT

Author Information
------------------

This role was created by @bmendesl 